<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_author}}`.
 */
class m201117_104736_create_book_author_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_author}}', [
            'id' => $this->primaryKey(),
            'author_id'=> $this->integer()->notNull(),
            'book_id'=>$this->integer()->notNull(),
        ]);
        $this->createIndex(
            'idx-author_id',
            '{{%book_author}}',
            'author_id'
        );
        $this->createIndex(
            'idx-book_id',
            '{{%book_author}}',
            'book_id'
        );
        $this->addForeignKey(
            'fk-author_id',
            '{{%book_author}}',
            'author_id',
            '{{%author}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'idx-book_id',
            '{{%book_author}}',
            'book_id',
            '{{%book}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_author}}');
    }
}
